#include <vector>
#include <iostream>
#include <list>
#include <array>
#include "src/FloatBucket.h"

int main() {
    FloatBucket* bucket = nullptr;

    std::string input = "";
    std::getline(std::cin, input);

    bucket = new FloatBucket(input);
    bucket->display();

    delete bucket;

    return 0;
}