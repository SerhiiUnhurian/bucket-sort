#include "FloatBucket.h"
#include "sstream"

FloatBucket::FloatBucket(std::string &input) {
    parseStr(input);
}

std::uint32_t FloatBucket::size() {
    return _size;
}

std::uint32_t FloatBucket::maxCollisions() {
    std::uint32_t maxCollisions {0};
    std::uint32_t numCollisions {0};

    for (int i = 0; i < _size; i++) {
        numCollisions = _bucket[i].size();

        if (numCollisions > maxCollisions) {
            maxCollisions = numCollisions;
        }
    }
    return maxCollisions;
}

void FloatBucket::add(float data) {
    if (data >= bucket_size) {
        return;
    }
    std::list<float>& bucketList = _bucket[(int)data];
    bool isInserted = false;

    for (auto it = bucketList.begin(); it != bucketList.end(); it++) {
        if ( data < *it) {
            bucketList.insert(it, data);
            _size++;
            isInserted = true;
            break;
        }
    }
    if (!isInserted) {
        bucketList.push_back(data);
        _size++;
    }
}

void FloatBucket::display() {
    for (int i = 0; i < bucket_size; i++) {
        const std::list<float>& bucketList = _bucket[i];

        for (auto it = bucketList.begin(); it != bucketList.end(); it++) {
            std::cout << *it << ' ';
        }
    }
    std::cout << '\n' << maxCollisions();
}

void FloatBucket::parseStr(const std::string &input) {
    std::stringstream ss;
    ss << input;

    std::string tmp;
    float data;

    while (!ss.eof()) {
        ss >> tmp;
        if (std::stringstream(tmp) >> data) {
            if (data == 0)
                break;
            add(data);
        }
    }
}