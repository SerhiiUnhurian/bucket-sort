/*
 *  FloatBucket class designed to sort and store floating point numbers in a rang from 0 to 100.
 *  Implemented using bucket sort algorithm.
 */

#ifndef FLOAT_BUCKET_SORT_H
#define FLOAT_BUCKET_SORT_H

#include <iostream>
#include <string>
#include <list>
#include "cstddef"

constexpr std::uint32_t bucket_size{100};

class FloatBucket {
private:
    std::list<float> _bucket[bucket_size];
    std::uint32_t _size {0};

    // Parses all values from the given string input. Input ends with number 0.
    void parseStr(const std::string &input);
public:
    FloatBucket(std::string& input);

    // Returns the number of values stored in the bucket.
    std::uint32_t size();
    // Returns maximum number of collisions in the bucket. That is the maximum number of values
    // that fall into one pocket.
    std::uint32_t maxCollisions();
    // Adds float value to the bucket.
    void add(float data);
    // Displays all values stored in the bucket in order from smallest to largest.
    void display();
};

#endif //FLOAT_BUCKET_SORT_H
